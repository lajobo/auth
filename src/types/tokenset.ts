import { Document, Model } from "mongoose";
import { User } from "./user";
import { Client } from "./client";

export interface Tokenset extends Document {
    userId: string;
    clientId: string;
    createdAt: Date;
    expiresAt: Date;
    accessToken: string;
    refreshToken: string;
    origin: TokensetOrigin;
    invalid: boolean;
    invalidationOrigin: TokensetInvalidationOrigin,
    invalidatedByTokenset: string;
    invalidatedByUser: string;
    invalidatedAt: Date;
    refresh: () => Promise<RefreshTokensetResult>;
    invalidateTokenset: (origin: TokensetInvalidationOrigin, originDetails?: string) => Promise<void>;
}

export interface TokensetModelType extends Model<Tokenset> {
    generate: (user: User, client: Client, origin: TokensetOrigin) => Promise<Tokenset>;
    invalidateTokensetsByUserId: (userId: string, invalidatingTokenset?: string) => Promise<void>;
    validateAccessToken: (authorization: string) => Promise<boolean>;
}

export enum RefreshTokensetStates {
    NotFound,
    Invalid,
    NotExpired,
    Refreshed
}

export interface RefreshTokensetResult {
    state: RefreshTokensetStates;
    tokenset?: Tokenset;
}

export enum TokensetOriginEnum {
    Login = "login"
}

export type TokensetOrigin = TokensetOriginEnum | string;

export enum TokensetInvalidationOrigin {
    Tokenset = "tokenset",
    Logout = "logout",
    // eslint-disable-next-line @typescript-eslint/no-shadow
    User = "user"
}
