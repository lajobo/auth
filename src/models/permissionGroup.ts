import { Schema } from "mongoose";
import { PermissionGroup } from "../types/permissionGroup";

export const PermissionGroupSchema: Schema<PermissionGroup> = new Schema<PermissionGroup>({
    clientId: String,
    permissions: [String]
});
