import { Model } from "mongoose";

export interface DeleteResult {
    ok?: number;
    n?: number;
    deletedCount?: number;
}

export type AnyModel = Model<any>; // eslint-disable-line @typescript-eslint/no-explicit-any
