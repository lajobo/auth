import express from "express";
import { TokensetModel } from "../../models/tokenset";

export const authorizationMiddleware: (
    request: express.Request,
    response: express.Response,
    next: () => void
) => void = async (
    request: express.Request,
    response: express.Response,
    next: () => void
) => {
    if (!await TokensetModel.validateAccessToken(request.get("Authorization"))) {
        response.type("application/json").status(401).json({ error: "unauthorized" });
        return;
    }

    next();
};
