import { Document, Model } from "mongoose";
import { PermissionGroup } from "./permissionGroup";

export interface User extends Document {
    username: string;
    passwordHash: string;
    createdAt: Date;
    active: boolean;
    permissions: PermissionGroup[];
    verify: (password: string) => Promise<boolean>;
}

export interface UserModelType extends Model<User> {
    hashPassword: (password: string) => Promise<string>;
}
