export interface PermissionGroup extends Document {
    clientId: string;
    permissions: string[];
}
