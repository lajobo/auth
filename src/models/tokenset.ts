import { model, Schema } from "mongoose";
import { readFileSync } from "fs";
import { sign } from "jsonwebtoken";
import ms from "ms";
import { isAfter } from "date-fns";
import {
    RefreshTokensetResult,
    RefreshTokensetStates,
    Tokenset,
    TokensetInvalidationOrigin,
    TokensetModelType,
    TokensetOrigin
} from "../types/tokenset";
import { User } from "../types/user";
import { Client } from "../types/client";
import { ClientModel } from "./client";
import { UserModel } from "./user";
import { PermissionGroup } from "../types/permissionGroup";

// eslint-disable-next-line import/no-mutable-exports
let TokensetModel: TokensetModelType;

const TokensetSchema: Schema<Tokenset, TokensetModelType> = new Schema<Tokenset, TokensetModelType>({
    userId: String,
    clientId: String,
    createdAt: Date,
    expiresAt: Date,
    accessToken: String,
    refreshToken: String,
    origin: String,
    invalid: Boolean,
    invalidationOrigin: {
        type: String,
        enum: TokensetInvalidationOrigin
    },
    invalidatedByTokenset: String,
    invalidatedByUser: String,
    invalidatedAt: Date
});

async function invalidateTokensetsByUserId(userId: string, invalidatingTokenset?: string): Promise<void> {
    await TokensetModel.updateMany(
        {
            _id: { $ne: invalidatingTokenset },
            userId,
            invalid: { $ne: true }
        },
        {
            $set: {
                invalid: true,
                invalidationOrigin: TokensetInvalidationOrigin.Tokenset,
                invalidatedAt: new Date(Date.now()),
                invalidatedByTokenset: invalidatingTokenset
            }
        }
    );
}

async function validateAccessToken(authorization: string): Promise<boolean> {
    const accessToken: string = (authorization || "").split(" ")?.[1];
    const tokenset: Tokenset = await TokensetModel.findOne({ accessToken, invalid: { $ne: false } });
    return Boolean(tokenset);
}

async function generate(
    user: User,
    client: Client,
    origin: TokensetOrigin
) {
    const authPrivateKey: string = readFileSync("src/assets/keys/auth_private.pem", "utf8");
    const permissionGroup: PermissionGroup = user.permissions.find(
        // eslint-disable-next-line no-underscore-dangle
        (permission: PermissionGroup) => permission.clientId === String(client._id)
    );

    const accessToken: string = sign(
        { permissions: permissionGroup?.permissions || [] },
        authPrivateKey,
        {
            algorithm: "RS256",
            expiresIn: process.env.ACCESS_TOKEN_TTL,
            issuer: process.env.JWT_ISSUER_NAME,
            audience: client.name
        }
    );

    const refreshToken: string = sign(
        {},
        authPrivateKey,
        {
            algorithm: "RS256",
            expiresIn: process.env.REFRESH_TOKEN_TTL,
            issuer: process.env.JWT_ISSUER_NAME,
            audience: client.name
        }
    );

    const now: number = Date.now();
    const createdAt: Date = new Date(now);
    const expiresAt: Date = new Date(now + ms(process.env.ACCESS_TOKEN_TTL));

    const tokenset: Tokenset = new TokensetModel({
        userId: user.id,
        clientId: client.id,
        createdAt,
        expiresAt,
        accessToken,
        refreshToken,
        origin
    });
    await tokenset.save();

    await invalidateTokensetsByUserId(user.id, tokenset.id);

    return tokenset;
}

async function refresh(this: Tokenset): Promise<RefreshTokensetResult> {
    if (this.invalid) {
        return { tokenset: this, state: RefreshTokensetStates.Invalid };
    }

    if (isAfter(this.expiresAt, new Date())) {
        return { tokenset: this, state: RefreshTokensetStates.NotExpired };
    }

    const client: Client = await ClientModel.findOne({ _id: this.clientId });
    const user: User = await UserModel.findOne({ _id: this.userId });

    const refreshedTokenset: Tokenset = await generate(user, client, this.id);

    await this.invalidateTokenset(TokensetInvalidationOrigin.Tokenset, refreshedTokenset.id);

    return { state: RefreshTokensetStates.Refreshed, tokenset: refreshedTokenset };
}

async function invalidateTokenset(
    this: Tokenset,
    origin: TokensetInvalidationOrigin,
    originDetails?: string
): Promise<void> {
    this.invalid = true;
    this.invalidationOrigin = origin;
    this.invalidatedAt = new Date(Date.now());

    if (origin === TokensetInvalidationOrigin.Tokenset && originDetails) {
        this.invalidatedByTokenset = originDetails;
    }

    if (origin === TokensetInvalidationOrigin.User && originDetails) {
        this.invalidatedByUser = originDetails;
    }

    await this.save();
}

TokensetSchema.statics.generate = generate;
TokensetSchema.statics.invalidateTokensetsByUserId = invalidateTokensetsByUserId;
TokensetSchema.statics.validateAccessToken = validateAccessToken;
TokensetSchema.methods.refresh = refresh;
TokensetSchema.methods.invalidateTokenset = invalidateTokenset;

TokensetModel = model<Tokenset, TokensetModelType>("Tokenset", TokensetSchema);

export { TokensetModel };
