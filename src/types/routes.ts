import * as core from "express";
import { AnyObject, Document } from "mongoose";
import { AnyModel } from "./model";

export type Route = (req: core.Request, res: core.Response) => void;
export type RouteCreator = () => Route;

export type CreateFilter = (rawData: AnyObject) => object;
export type ReadFilter<T = Document> = (rawData: T) => object;
export type UpdateFilter = (document: Document, rawData: AnyObject) => object;

export type CreateRouteCreator = (model: AnyModel, createFilter: CreateFilter) => Route;
export type ReadRouteCreator = (model: AnyModel, readFilter?: ReadFilter) => Route;
export type UpdateRouteCreator = (model: AnyModel, updateFilter: UpdateFilter) => Route;
export type DeleteRouteCreator = (model: AnyModel) => Route;
