import { format } from "date-fns";
import express from "express";
import { log } from "../../utils/logger";
import { LogLevels } from "../../types/logger";

export const accessLogMiddleware: (request: express.Request, response: express.Response, next: () => void) => void = (
    request: express.Request,
    response: express.Response,
    next: () => void
) => {
    const sendLog: () => void = () => {
        response.removeListener("finish", sendLog);
        response.removeListener("close", sendLog);

        log(
            LogLevels.Access,
            // eslint-disable-next-line max-len
            `${format(new Date(), "yyyy-MM-dd HH-mm-ss")} ${request.method.padEnd(4, " ")} ${request.url} => [${response.statusCode}] ${response.statusMessage}`
        );
    };

    response.on("finish", sendLog);
    response.on("close", sendLog);

    next();
};
