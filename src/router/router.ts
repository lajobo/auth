import * as core from "express-serve-static-core";
import express, { NextFunction, Request, Response } from "express";
import bodyParser from "body-parser";
import compression from "compression";
import { log } from "../utils/logger";
import { LogLevels } from "../types/logger";
import { errorRoute } from "../controllers/error";
import { accessLogMiddleware } from "./middlewares/accessLog";
import { auth, invalidate, refresh } from "../controllers/ropc";
import { registerUserRoutes } from "../controllers/user";
import { databaseConnectionMiddleware } from "./middlewares/databaseConnection";
import { authorizationMiddleware } from "./middlewares/authorization";
import { clientCredentialsMiddleware } from "./middlewares/clientCredentials";

export const initializeRoutes: (checkDatabaseConnection: () => boolean) => void = (
    checkDatabaseConnection: () => boolean
) => {
    const app: core.Express = express();

    app.use(compression());
    app.use(express.json());
    app.use(accessLogMiddleware);
    app.use(databaseConnectionMiddleware(checkDatabaseConnection));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));

    app.disable("x-powered-by");

    app.post("/auth", auth);
    app.post("/refresh", refresh);
    app.post("/invalidate", invalidate);

    app.use(authorizationMiddleware); // authorization required after this point
    app.use(clientCredentialsMiddleware); // clientId and clientSecret required after this point

    registerUserRoutes(app);

    // Express requires 4 arguments in the route to detect it as an error route
    /* eslint-disable @typescript-eslint/no-unused-vars */
    // noinspection JSUnusedLocalSymbols
    app.use((error: Error, req: Request, res: Response, next: NextFunction) => {
        errorRoute(req, res, error);
    });
    /* eslint-enable @typescript-eslint/no-unused-vars */

    app.listen(process.env.PORT, () => {
        log(LogLevels.Info, `Webserver started at port ${process.env.PORT}`);
    });
};
