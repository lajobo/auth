import { Document } from "mongoose";

export interface Client extends Document {
    name: string;
    url: string;
    clientId: string;
    publicKey: string;
    privateKey: string;
    active: boolean;
    permissions: string[];
    authenticate: (clientSecret: string) => boolean;
}
