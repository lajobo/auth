export const forceArray: <T = unknown>(data: unknown | unknown[]) => T[] =
    (data: unknown | unknown[]) => (Array.isArray(data) ? data : [data]);
