import * as core from "express";
import { RouteCreator } from "../types/routes";

export const registerCRUDRoutes: (
    app: core.Express,
    path: string,
    endpoints: {
        createRoute?: RouteCreator,
        readRoute?: RouteCreator,
        updateRoute?: RouteCreator,
        deleteRoute?: RouteCreator
    }
) => void = (
    app: core.Express,
    path: string,
    endpoints: {
        createRoute?: RouteCreator,
        readRoute?: RouteCreator,
        updateRoute?: RouteCreator,
        deleteRoute?: RouteCreator
    }
) => {
    const prefixedPath: string = path.startsWith("/") ? path : `/${path}`;

    if (endpoints?.createRoute) {
        app.post(prefixedPath, endpoints.createRoute());
    }

    if (endpoints?.readRoute) {
        app.get(prefixedPath, endpoints.readRoute());
        app.get(`${prefixedPath}/:id`, endpoints.readRoute());
    }

    if (endpoints?.updateRoute) {
        app.post(prefixedPath, endpoints.updateRoute());
        app.put(prefixedPath, endpoints.updateRoute());
    }

    if (endpoints?.deleteRoute) {
        app.delete(`${prefixedPath}/:id`, endpoints.deleteRoute());
    }
};
