import crypto from "crypto";

export const sign: (input: string, privateKey: string) => string = (input: string, privateKey: string) => crypto.sign(
    "sha256",
    Buffer.from(input),
    privateKey
).toString("base64");

export const verify: (signature: string, clientId: string, publicKey: string) => boolean = (
    signature: string,
    clientId: string,
    publicKey: string
) => crypto.verify(
    "sha256",
    Buffer.from(clientId),
    publicKey,
    Buffer.from(signature, "base64")
);
