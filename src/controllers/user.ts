import { AnyObject } from "mongoose";
import * as core from "express";
import { User } from "../types/user";
import { UserModel } from "../models/user";
import { RouteCreator } from "../types/routes";
import { registerCRUDRoutes } from "../utils/route";
import { genericCreateRoute, genericDeleteRoute, genericReadRoute, genericUpdateRoute } from "./crud";

export const createRoute: RouteCreator = () => genericCreateRoute(
    UserModel,
    async (rawData: AnyObject) => ({
        username: rawData?.username,
        passwordHash: await UserModel.hashPassword(rawData?.password),
        createdAt: rawData?.createdAt,
        active: rawData?.active,
        permissions: []
    })
);

export const readRoute: RouteCreator = () => genericReadRoute(
    UserModel,
    (user: User) => ({
        id: user.id,
        username: user.username,
        createdAt: user.createdAt,
        active: user.active
    })
);

export const updateRoute: RouteCreator = () => genericUpdateRoute(
    UserModel,
    async (user: User, rawData: AnyObject) => ({
        username: rawData.username,
        passwordHash: rawData?.password ? await UserModel.hashPassword(rawData?.password) : user.passwordHash,
        active: rawData.active
    })
);

export const deleteRoute: RouteCreator = () => genericDeleteRoute(UserModel);

export const registerUserRoutes: (app: core.Express) => void = (app: core.Express) => {
    registerCRUDRoutes(app, "users", {
        createRoute, readRoute, updateRoute, deleteRoute
    });
};
