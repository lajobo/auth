import { Envs } from "../types/env";

export const isProduction: boolean = process.env.NODE_ENV === Envs.Production;
