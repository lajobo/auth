import { LogLevels } from "../types/logger";
import i18n from "../i18n/i18n.json";

export const log: (level: LogLevels, ...messages: unknown[]) => void = (level: LogLevels, ...messages: unknown[]) => {
    if (level <= (Number(process.env.LOG_LEVEL) || 0)) {
        // eslint-disable-next-line no-console
        console.log(`[${i18n.log[level]}]`, ...messages);
    }
};
