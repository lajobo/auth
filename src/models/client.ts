import { model, Model, Schema } from "mongoose";
import { Client } from "../types/client";
import { verify } from "../utils/crypto";

const ClientSchema: Schema<Client> = new Schema<Client>({
    name: String,
    url: String,
    clientId: String,
    publicKey: String,
    privateKey: String,
    active: Boolean,
    permissions: [String]
});

function authenticate(this: Client, clientSecret: string): boolean {
    return this.active && verify(clientSecret, this.clientId, this.publicKey);
}

ClientSchema.methods.authenticate = authenticate;

export const ClientModel: Model<Client> = model<Client>("Client", ClientSchema);
