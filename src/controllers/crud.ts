import { Request, Response } from "express";
import { Document, Error } from "mongoose";
import {
    CreateFilter,
    CreateRouteCreator,
    DeleteRouteCreator,
    ReadFilter,
    ReadRouteCreator,
    UpdateFilter,
    UpdateRouteCreator
} from "../types/routes";
import { AnyModel, DeleteResult } from "../types/model";
import { forceArray } from "../utils/array";
import { isValidObjectId } from "../utils/model";

export const genericCreateRoute: CreateRouteCreator = (
    model: AnyModel,
    filter: CreateFilter
) => async (
    req: Request,
    res: Response
) => {
    if (req.params.id) {
        res.type("application/json").status(400).json({ error: "bad_request" });
        return;
    }

    try {
        const result: Document = await model.create(await filter(req.body));

        res
            .type("application/json")
            .status(204)
            .send(result.id);
    } catch (err: any) { // eslint-disable-line @typescript-eslint/no-explicit-any
        const errors: { field: string, error: string }[] = (err.errors ? Object.values(err.errors) : [])
            .map((error: Error.ValidatorError) => ({
                field: error.path,
                error: error.kind
            }));

        if (err.code === 11000) {
            errors.push({
                field: Object.keys(err?.keyPattern || {})?.[0],
                error: "unique"
            });
        }

        res
            .type("application/json")
            .status(409)
            .send(errors);
    }
};

export const genericReadRoute: ReadRouteCreator = (
    model: AnyModel,
    filter?: ReadFilter
) => async (
    req: Request,
    res: Response
) => {
    const id: string | undefined = req.params?.id;
    const documents: Document[] = id
        ? forceArray(await model.findOne({ _id: id }))
        : await model.find();

    const data: object[] =
        documents.map((document: Document) => (filter ? filter(document) : document.toObject()));

    res
        .type("application/json")
        .status(200)
        .json(id ? data?.[0] : data);
};

export const genericUpdateRoute: UpdateRouteCreator = (
    model: AnyModel,
    filter: UpdateFilter
) => async (
    req: Request,
    res: Response
) => {
    if (!req.body?.id) {
        res.type("application/json").status(400).json({ error: "bad_request" });
        return;
    }

    const { id, ...data } = req.body;
    const document: Document = await model.findOne({ _id: id });

    if (!document) {
        res.type("application/json").status(404).json({ error: "not_found" });
        return;
    }

    document.updateOne(
        await filter(document, data),
        null,
        (err: any) => { // eslint-disable-line @typescript-eslint/no-explicit-any
            if (err) {
                if (err.code === 11000) {
                    res
                        .type("application/json")
                        .status(409)
                        .send([{ field: Object.keys(err?.keyPattern || {})?.[0], error: "unique" }]);
                } else {
                    res
                        .type("application/json")
                        .status(500)
                        .send({ error: "internal_server_error" });
                }
            } else {
                res.status(204).send();
            }
        }
    );
};

export const genericDeleteRoute: DeleteRouteCreator = (model: AnyModel) => async (req: Request, res: Response) => {
    if (!isValidObjectId(req.params?.id)) {
        res.type("application/json").status(400).json({ error: "bad_request" });
        return;
    }

    const result: DeleteResult = await model.deleteOne({ _id: req.params?.id });
    const success: boolean = result.n === 1 && result.deletedCount === 1;

    res
        .type("application/json")
        .status(success ? 204 : 404)
        .send();
};
