export enum LogLevels {
    Off,
    Fatal,
    Error,
    Warn,
    Info,
    Access,
    Debug,
    Trace
}
