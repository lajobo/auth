import express from "express";

export const databaseConnectionMiddleware: (
    checkDatabaseConnection: () => boolean
) => (
    request: express.Request,
    response: express.Response,
    next: () => void
) => void = (
    checkDatabaseConnection: () => boolean
) => (
    request: express.Request,
    response: express.Response,
    next: () => void
) => {
    if (!checkDatabaseConnection()) {
        response.type("application/json").status(500).json({ error: "internal_server_error" });
        return;
    }

    next();
};
