# OAuth2/Auth

OAuth2 is an authorization protocol. More information about the protocol itself can be found on the [official website](https://oauth.net/2/).

## Quick start
Rename `/example.env` to `/.env` and fill in the placeholder values. You will need a mongo db to run this service.
```bash
npm install
```
followed by
```bash
npm run start
```

## Usage
This service will provide a basic implementation of the OAuth2 protocol. It will allow clients (other services) to authorize a user.
At the same time it provides CRUD endpoints to manage the clients and users.

## Extensibility
This project was started as a learning project to get deeper into the OAuth2 protocol.
The basic implementation is done, and it can be used by different services to authorize.
Therefore, it is considered to be done for the moment.
Anyhow, it could easily be extended to handle more tasks or extend the OAuth token e.g. to allow a single user to be logged in on multiple devices at a time.

## Motivation
This project was started after I became part of a project at work in which I had to send requests to an existing OAuth2 authorization service.
During the project meetings I have realized that even though I know the basics about the protocol, I could not really contribute to in depth questions.
So I sat down and started reading about the protocol to be prepared for the next meeting.
That worked out pretty well and also hooked me up to have my own version running which led to the development of this project.

After I have started with the implementation I was in need of a database to store the data in.
Instead of using a relational database, like so many times before, I wanted to try a document based database for this project.
I want to highlight at this point that MongoDB was not necessarily the technically better decision, but it was solely for the purpose of learning.

#### Author

* [Lars Bomnüter](mailto:larsbomnueter@web.de)

---

MIT License
