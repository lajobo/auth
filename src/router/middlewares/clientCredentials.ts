import express from "express";
import { Client } from "../../types/client";
import { ClientModel } from "../../models/client";

export const clientCredentialsMiddleware: (
    request: express.Request,
    response: express.Response,
    next: () => void
) => void = async (
    request: express.Request,
    response: express.Response,
    next: () => void
) => {
    const clientId: string = request.get("X-Client-Id");
    const clientSecret: string = request.get("X-Client-Secret");

    if (
        [clientId, clientSecret].includes(undefined) ||
        [clientId.length, clientSecret.length].includes(0)
    ) {
        response.type("application/json").status(400).json({ error: "bad_request" });
        return;
    }

    const client: Client = await ClientModel.findOne({ clientId });

    if (!client || !client.authenticate(clientSecret)) {
        response.type("application/json").status(400).json({ error: "invalid_client" });
        return;
    }

    next();
};
