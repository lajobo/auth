import { Request, Response } from "express";
import { ClientModel } from "../models/client";
import { UserModel } from "../models/user";
import { User } from "../types/user";
import { TokensetModel } from "../models/tokenset";
import {
    RefreshTokensetResult,
    RefreshTokensetStates,
    Tokenset,
    TokensetInvalidationOrigin,
    TokensetOriginEnum
} from "../types/tokenset";
import { Client } from "../types/client";
import { Route } from "../types/routes";

export const auth: Route = async (req: Request, res: Response) => {
    const grantType: string = req.body?.grant_type;
    const username: string = req.body?.username;
    const password: string = req.body?.password;
    const clientId: string = req.body?.client_id;
    const clientSecret: string = req.body?.client_secret;

    if (
        grantType !== "password" ||
        [username, password, clientId, clientSecret].includes(undefined) ||
        [username.length, password.length, clientId.length, clientSecret.length].includes(0)
    ) {
        res.type("application/json").status(400).json({ error: "bad_request" });
        return;
    }

    const client: Client = await ClientModel.findOne({ clientId });

    if (!client || !client.authenticate(clientSecret)) {
        res.type("application/json").status(400).json({ error: "invalid_client" });
        return;
    }

    const user: User = await UserModel.findOne({ username });

    if (!user || !await user.verify(password)) {
        res.type("application/json").status(401).json({ error: "access_denied" });
        return;
    }

    const tokenset: Tokenset = await TokensetModel.generate(user, client, TokensetOriginEnum.Login);

    res
        .type("application/json")
        .status(201)
        .json({ accessToken: tokenset.accessToken, refreshToken: tokenset.refreshToken });
};

export const refresh: Route = async (req: Request, res: Response) => {
    const grantType: string = req.body?.grant_type;
    const refreshToken: string = req.body?.refreshToken;
    const clientId: string = req.body?.client_id;
    const clientSecret: string = req.body?.client_secret;

    if (
        grantType !== "password" ||
        [refreshToken, clientId, clientSecret].includes(undefined) ||
        [refreshToken.length, clientId.length, clientSecret.length].includes(0)
    ) {
        res.type("application/json").status(400).json({ error: "bad_request" });
        return;
    }

    const client: Client = await ClientModel.findOne({ clientId });

    if (!client || !client.authenticate(clientSecret)) {
        res.type("application/json").status(400).json({ error: "invalid_client" });
        return;
    }

    // eslint-disable-next-line no-underscore-dangle
    const tokenset: Tokenset = await TokensetModel.findOne({ refreshToken, clientId: client._id });

    if (!tokenset) {
        res.type("application/json").status(401).json({ error: "invalid_token" });
        return;
    }

    const refreshTokensetResult: RefreshTokensetResult = await tokenset.refresh();

    switch (refreshTokensetResult.state) {
        case RefreshTokensetStates.Invalid:
            res.type("application/json").status(401).json({ error: "invalid_token" });
            return;
        case RefreshTokensetStates.NotExpired:
            res
                .type("application/json")
                .status(200)
                .json({
                    accessToken: refreshTokensetResult.tokenset.accessToken,
                    refreshToken: refreshTokensetResult.tokenset.refreshToken
                });
            return;
        case RefreshTokensetStates.Refreshed:
            res
                .type("application/json")
                .status(201)
                .json({
                    accessToken: refreshTokensetResult.tokenset.accessToken,
                    refreshToken: refreshTokensetResult.tokenset.refreshToken
                });
            return;
    }

    res.sendStatus(500);
};

export const invalidate: Route = async (req: Request, res: Response) => {
    const grantType: string = req.body?.grant_type;
    const refreshToken: string = req.body?.refreshToken;
    const clientId: string = req.body?.client_id;
    const clientSecret: string = req.body?.client_secret;

    if (
        grantType !== "password" ||
        [refreshToken, clientId, clientSecret].includes(undefined) ||
        [refreshToken.length, clientId.length, clientSecret.length].includes(0)
    ) {
        res.type("application/json").status(400).json({ error: "bad_request" });
        return;
    }

    const client: Client = await ClientModel.findOne({ clientId });

    if (!client || !client.authenticate(clientSecret)) {
        res.type("application/json").status(400).json({ error: "invalid_client" });
        return;
    }

    const tokenset: Tokenset = await TokensetModel.findOne({ refreshToken });

    if (tokenset) {
        await tokenset.invalidateTokenset(TokensetInvalidationOrigin.Logout);
    }

    res.sendStatus(200);
};
