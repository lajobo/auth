import { model, Schema } from "mongoose";
import bcrypt from "bcrypt";
import { User, UserModelType } from "../types/user";
import { PermissionGroupSchema } from "./permissionGroup";

// eslint-disable-next-line import/no-mutable-exports
let UserModel: UserModelType;

const UserSchema: Schema<User, UserModelType> = new Schema<User, UserModelType>({
    username: { type: String, unique: true, required: true },
    passwordHash: { type: String, required: true },
    createdAt: { type: Date, required: true },
    active: Boolean,
    permissions: [PermissionGroupSchema]
});

async function verify(this: User, password: string): Promise<boolean> {
    let valid: boolean = false;

    if (this.active) {
        valid = await bcrypt.compare(password, this.passwordHash);
    }

    return valid;
}

async function hashPassword(password: string): Promise<string> {
    return new Promise<string>((resolve, reject) => {
        bcrypt.hash(password, 10, (err, hash) => {
            if (!err && hash) {
                resolve(hash);
            } else {
                reject(err);
            }
        });
    });
}

UserSchema.statics.hashPassword = hashPassword;
UserSchema.methods.verify = verify;

// eslint-disable-next-line prefer-const
UserModel = model<User, UserModelType>("User", UserSchema);

export { UserModel };
