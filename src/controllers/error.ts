import { Request, Response } from "express";
import { log } from "../utils/logger";
import { LogLevels } from "../types/logger";
import { isProduction } from "../utils/env";

export const errorRoute: (req: Request, res: Response, error: Error) => void = (
    req: Request,
    res: Response,
    error: Error
) => {
    log(LogLevels.Error, `Routing error\n${error}`);

    if (!isProduction) {
        res.status(500).json({ error: error.toString() });
    } else {
        res.status(500).send();
    }
};
