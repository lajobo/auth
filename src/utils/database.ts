import { connect, connection } from "mongoose";
import { log } from "./logger";
import { LogLevels } from "../types/logger";
import Timeout = NodeJS.Timeout;

export const initDatabase: () => Promise<() => boolean> = () => (
    new Promise<() => boolean>((resolve, reject) => {
        let connected: boolean = false;

        connect(
            process.env.DB_HOST,
            {
                useNewUrlParser: true,
                useUnifiedTopology: true,
                keepAlive: true,
                keepAliveInitialDelay: 300000,
                user: process.env.DB_USER,
                pass: process.env.DB_PASSWORD,
                dbName: process.env.DB_NAME
            }
        )
            .then(() => { /* do nothing */ })
            .catch(() => { /* do nothing */ });

        const timeoutId: Timeout = setTimeout(() => {
            connected = false;
            log(LogLevels.Fatal, "Database connection timed out");
            reject(new Error("Database connection timed out"));
        }, 10000);

        connection.on("error", (error: unknown) => {
            connected = false;
            log(LogLevels.Fatal, "Database connection error", error);
        });

        connection.once("open", () => {
            connected = true;
            log(LogLevels.Info, "Database connection established");
            clearTimeout(timeoutId);
            resolve(() => connected);
        });

        connection.on("close", () => {
            connected = false;
            log(LogLevels.Fatal, "Database connection closed");
        });

        connection.on("disconnected", () => {
            connected = false;
            log(LogLevels.Fatal, "Database connection disconnected");
        });

        connection.on("reconnected", () => {
            connected = true;
            log(LogLevels.Info, "Database connection reconnected");
        });

        return () => connected;
    })
);
