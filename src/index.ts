import { config as setupDotenv } from "dotenv";
import { initDatabase } from "./utils/database";
import { log } from "./utils/logger";
import { LogLevels } from "./types/logger";
import { initializeRoutes } from "./router/router";

setupDotenv();
initDatabase()
    .then(async (checkDatabaseConnection: () => boolean) => {
        log(LogLevels.Info, "Database connection initialization succeeded", checkDatabaseConnection());
        initializeRoutes(checkDatabaseConnection);
    })
    .catch(() => {
        log(LogLevels.Fatal, "Database connection initialization failed");
    });
