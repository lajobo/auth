export const isValidObjectId: (id: string) => boolean = (id: string) => Boolean(id.match(/^[0-9a-fA-F]{24}$/));
